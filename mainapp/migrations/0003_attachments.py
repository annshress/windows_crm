# Generated by Django 2.2.5 on 2020-02-22 11:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0002_customer_mobile'),
    ]

    operations = [
        migrations.CreateModel(
            name='Attachments',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file_type', models.CharField(max_length=255)),
                ('file_link', models.CharField(max_length=1000)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('is_active', models.BooleanField(default=True, verbose_name='active')),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='attachments', to='mainapp.Customer')),
            ],
        ),
    ]
